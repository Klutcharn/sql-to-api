﻿namespace WebAPI.Models.DTO.Franchise
{
    public class CreateFranchiseDTO
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
