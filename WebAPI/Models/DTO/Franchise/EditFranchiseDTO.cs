﻿namespace WebAPI.Models.DTO.Franchise
{
    public class EditFranchiseDTO
    {
        public string Name { get; set; }

        public string Description { get; set; }

    }
}
