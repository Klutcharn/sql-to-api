﻿namespace WebAPI.Models.DTO.Franchise
{
    public class ReadFranchiseWithMovieDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public List<int> MovieIds { get; set; }


    }
}
