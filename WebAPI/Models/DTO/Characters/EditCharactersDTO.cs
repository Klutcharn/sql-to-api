﻿namespace WebAPI.Models.DTO.Characters
{
    public class EditCharactersDTO
    {
        public string Name { get; set; }

        public string alias { get; set; }

        public string gender { get; set; }

        public string Picture { get; set; }
    }
}
