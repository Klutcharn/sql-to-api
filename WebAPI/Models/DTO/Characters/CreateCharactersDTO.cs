﻿namespace WebAPI.Models.DTO.Characters
{
    public class CreateCharactersDTO
    {
        public string Name { get; set; }

        public string alias { get; set; }

        public string gender { get; set; }

        public string Picture { get; set; }
    }
}
