﻿using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models
{
    public class Franchise
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(50)]

        public string Name { get; set; }
        [MaxLength(50)]

        public string Description { get; set; }

        public ICollection<Movie> Movies { get; set; }


    }
}
