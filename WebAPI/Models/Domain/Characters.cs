﻿using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models
{
    public class Characters
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string alias { get; set; }

        public string gender { get; set; }

        public string Picture { get; set; }

        public ICollection<Movie> Movies { get; set; }
    }
}
