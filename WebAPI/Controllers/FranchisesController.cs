﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data;
using WebAPI.Models;
using WebAPI.Models.DTO;
using WebAPI.Models.DTO.Franchise;

namespace WebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MoviesContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MoviesContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets a list of franchise
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadFranchiseDTO>>> GetFranchises()
        {

            //Gonna fix this.
            var franchiseList = _mapper.Map<List<ReadFranchiseDTO>>(await _context.Franchises.ToArrayAsync());
            return franchiseList;
        }

        /// <summary>
        /// Gets a list of franchise with all MovieIds that are included in each franchise
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadFranchiseWithMovieDTO>>> GetFranchisesWithMovies()
        {

            //Gonna fix this.
         //   var loaded = _context.Franchises.Include(p => p.Movies).ToList<Franchise>();
            var franchiseList = _mapper.Map<List<ReadFranchiseWithMovieDTO>>(await _context.Franchises.Include(p => p.Movies).ToListAsync<Franchise>());
            
            return Ok(franchiseList);
        }

        //hej där

        /// <summary>
        /// Gets a list of franchise with all CharacterIds that are included in each franchise
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadFranchiseWithCharactersDTO>>> GetFranchisesWithCharacters()
        {

            //Gonna fix this.
            //   var loaded = _context.Franchises.Include(p => p.Movies).ToList<Franchise>();
            var franchiseList = _mapper.Map<List<ReadFranchiseWithCharactersDTO>>(await _context.Franchises.Include(p => p.Movies).ToListAsync<Franchise>());

            return Ok(franchiseList);
        }




        /// <summary>
        /// Gets a specific Franchise based on the Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadFranchiseDTO>> GetFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map< ReadFranchiseDTO> (franchise);
        }




        /// <summary>
        /// Updates a specific Franchise based on the Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, ReadFranchiseDTO dtoFranchise)
        {
            if (id != dtoFranchise.Id)
            {
                return BadRequest();
            }
            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);

            _context.Entry(domainFranchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        /// <summary>
        /// Creates a new Franchise.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise( CreateFranchiseDTO dtoFranchise)
        {
            Franchise franchiseDomain = _mapper.Map<Franchise>(dtoFranchise);
            _context.Franchises.Add(franchiseDomain);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise",
                new { id = franchiseDomain.Id },
                _mapper.Map<CreateFranchiseDTO>(franchiseDomain));
        }


        /// <summary>
        /// Deletes a specific Franchise based on the Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }






    }
}
