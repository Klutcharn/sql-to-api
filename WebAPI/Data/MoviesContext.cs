﻿using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Data
{
    public class MoviesContext : DbContext
    {
        public DbSet<Characters> Characters { get; set; }

        public DbSet<Franchise> Franchises { get; set; }

        public DbSet<Movie> Movies { get; set; }


        public MoviesContext(DbContextOptions options) : base(options)
        {

        }
        //detta ska fixas auto sen när vi lär oss
 
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
          
        //}
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Characters>()            
                .HasData(new Characters
                {
                    Id = 1,
                    Name = "Thomas Clooney",
                    alias = "Danger Mouse",
                    gender = "Male",
                    //   Movies = new List<Movie>() { new Movie() { MovieId = 1 } },
                    Picture = "https://www.zingland.se/img/c6/43/52/e8/400x400/dundermusen-maskeraddrakt-barn.jpg"
                });
            modelBuilder.Entity<Characters>()
                .HasData(new Characters
                {
                    Id = 2,
                    Name = "Jocke Holland",
                    alias = "Spidey boi",
                    gender = "Female",
                    //   Movies = new List<Movie>() { new Movie() { MovieId = 2 } },
                    Picture = "https://pbs.twimg.com/profile_images/1205711832276750336/2E8C6Hiq_400x400.jpg"
                });
            modelBuilder.Entity<Characters>()
                .HasData(new Characters
                {
                    Id = 3,
                    Name = "Cat the Cat",
                    alias = "Danger Cat",
                    gender = "Male",
                    //    Movies = new List<Movie>() { new Movie() { MovieId = 1 } },
                    Picture = "https://images.fineartamerica.com/images/artworkimages/mediumlarge/3/bongo-cat-galo-dimitri.jpg"
                });



            modelBuilder.Entity<Franchise>()
              .HasData(new Franchise
              {
                  Id = 1,
                  Name = "Mouse movies",
                  Description = "Big cats go on mouse",
                
              }); ;
            modelBuilder.Entity<Franchise>()
              .HasData(new Franchise
              {
                  Id = 2,
                  Name = "Scary movies",
                  Description = "A mix of spider man and horror movie",
                
              });



            modelBuilder.Entity<Movie>()
               .HasData(new Movie
               {
                   Id = 1,
                   MovieTitle = "Danger mouse the danger movie",
                   Genre = "Action, Romance, Horror",
                   Director = "Baty Bateron",
                   ReleaseYear = 1937,
                   Trailer = "https://www.youtube.com/watch?v=tfMTHIwTUXA",
                   Picture = "https://i.discogs.com/VICgacRPYpdbeyiUv3sNvrIS4yAlx5tY5-CkGG7S7kc/rs:fit/g:sm/q:90/h:600/w:600/czM6Ly9kaXNjb2dz/LWltYWdlcy9BLTk3/MDMyMC0xNDk0MjM2/MDg1LTgwNDAuanBl/Zw.jpeg",
                   FranchiseId = 1
               });
            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 2,
                    MovieTitle = "Scary Grandma",
                    Genre = "Romance, Horror",
                    Director = "Old man",
                    ReleaseYear = 2022,
                    Trailer = "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
                    Picture = "https://www.scarymommy.com/wp-content/uploads/2015/07/scary-grandma-0.jpg",
                    FranchiseId = 2

                });
            //This no work

            //modelBuilder.Entity<Movie>()
            //    .HasMany(p => p.Characters)
            //    .WithMany(p => p.Movies)
            //    .UsingEntity<Dictionary<string, object>>(
            //        "CharacterMovies",
            //        r => r.HasOne<Character>().WithMany().HasForeignKey("CharactersCharacterid"),
            //        l => l.HasOne<Movie>().WithMany().HasForeignKey("MoviesMoviesId"),
            //        je =>
            //        {
            //            je.HasKey("CharactersCharacterid", "MoviesMoviesId");
            //            je.HasData(new { CharactersCharacterid = 1, MoviesMoviesId = 1 },
            //                new { CharactersCharacterid = 2, MoviesMoviesId = 2 },
            //                new { CharactersCharacterid = 3, MoviesMoviesId = 1 }
            //                );
            //        }
            //    );

            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(p => p.Movies)
                .UsingEntity(j => j.HasData(new { CharactersId = 1, MoviesId = 1 },
                new { CharactersId = 2, MoviesId = 2 },
                new { CharactersId = 3, MoviesId = 1 }));

        }
    }
}
