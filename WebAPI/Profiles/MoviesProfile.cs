﻿using AutoMapper;
using WebAPI.Models;
using WebAPI.Models.DTO;
using WebAPI.Models.DTO.Franchise;

namespace WebAPI.Profiles
{
    public class MoviesProfile : Profile
    {
        public MoviesProfile()
        {
            CreateMap<Movie,ReadMoviesDTO>();
            CreateMap<Movie, CreateMoviesDTO>();
            CreateMap<CreateMoviesDTO, Movie>();
            CreateMap<ReadMoviesDTO, Movie>();
        }
    }
}
