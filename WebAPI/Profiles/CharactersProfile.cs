﻿using AutoMapper;
using WebAPI.Models;
using WebAPI.Models.DTO.Characters;

namespace WebAPI.Profiles
{
    public class CharactersProfile:Profile
    {
        public CharactersProfile()
        {
            CreateMap<Characters, ReadCharactersDTO>();
            CreateMap<Characters, CreateCharactersDTO>();
            CreateMap<CreateCharactersDTO, Characters>();
            CreateMap<ReadCharactersDTO, Characters>();
        }
    }
    // fan va gött dö
}
