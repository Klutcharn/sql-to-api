﻿using AutoMapper;
using WebAPI.Models;
using WebAPI.Models.DTO;
using WebAPI.Models.DTO.Franchise;

namespace WebAPI.Profiles
{
    public class FranchiseProfile: Profile
    {



        public FranchiseProfile()
        {
            CreateMap<Franchise, ReadFranchiseDTO>()
                .ReverseMap();
            CreateMap<Franchise, CreateFranchiseDTO>()
                .ReverseMap();


            CreateMap<Franchise, ReadFranchiseWithMovieDTO>()
              .ForMember(pdto => pdto.MovieIds, opt =>
               opt.MapFrom(p => p.Movies.Select(s => s.Id).ToArray()))
              .ReverseMap();


            CreateMap<Franchise, ReadFranchiseWithCharactersDTO>()
            .ForMember(pdto => pdto.CharacterIds, opt =>
             opt.MapFrom(p => p.Movies.Select(s => s.Characters.Select(c => c.Id)).ToArray()))
            .ReverseMap();
        }
    }
}
